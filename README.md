## Name
UBER like service, in REST style

## Description
This application helps people to deliver their stuff and helps drivers to handle loads.

## Installation
Requirements: Node v18.7.0.

- Clone the repository
```
git clone  https://gitlab.com/dragosvasile/nodejs-hw03
```
- Install dependencies
```
cd nodejs-hw03
npm install
```
- Build and run the project
```
npm start
```
  Navigate to `http://localhost:8080`

- API Document endpoints

Can be found in project root folder -> 'openapi.yaml'

