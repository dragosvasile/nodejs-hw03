const writeXlsxFile = require('write-excel-file/node');
const Load = require('../models/load');


const generateReport = async (shippedLoads) => {
  const data = generateBody(shippedLoads, [generateHeader()]);
  const stream = await writeXlsxFile(data);
  return stream;
};

const generateHeader = () => {
  // excel headers
  const HEADER_ROW = [];
  HEADER_ROW.push({
    value: 'Id',
    fontWeight: 'bold',
  });
  for (const property in Load.schema.obj ) {
    if (property) {
      // destructuring dimensions property
      // to create rows for each inner property
      if (property === 'dimensions') {
        for (const dimensionsProp in Load.schema.obj[property]) {
          if (dimensionsProp) {
            HEADER_ROW.push({
              value: dimensionsProp,
              fontWeight: 'bold',
            });
          }
        }
      // destructuring logs property
      // to create rows for each inner property
      } else if ( property === 'logs') {
        for (const logs of Load.schema.obj[property]) {
          for (const logsProp in logs) {
            if (logsProp) {
              HEADER_ROW.push({
                value: property + ': ' + logsProp,
                fontWeight: 'bold',
              });
            }
          }
        }
      } else {
        HEADER_ROW.push({
          value: property,
          fontWeight: 'bold',
        });
      }
    }
  }
  return HEADER_ROW;
};

const generateBody = (shippedLoads, data) => {
  shippedLoads.forEach((load) => {
    const DATA_ROW = [];
    DATA_ROW.push({
      value: load._id,
    });
    for (const property in Load.schema.obj) {
      if (property) {
        if (property === 'dimensions') {
          for (const dimensionsProp in Load.schema.obj[property]) {
            if (dimensionsProp) {
              DATA_ROW.push({
                value: load[property][dimensionsProp],
              });
            }
          }
        } else if ( property === 'logs' && load[property].length > 0) {
          for (const logs of Load.schema.obj[property]) {
            for (const logsProp in logs) {
              if (logsProp) {
                DATA_ROW.push({
                  value: 'Last log: ' + load['logs'][0][logsProp],
                });
              }
            }
          }
        } else {
          DATA_ROW.push({
            value: load[property],
            format: property === 'created_date' ? 'mm/dd/yyyy' : null,
          });
        }
      }
    }
    data.push(DATA_ROW);
  });
  return data;
};


module.exports = {generateReport};
