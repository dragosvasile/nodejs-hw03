const Joi = require('joi');

const idSchema = Joi.object({
  id: Joi.string().alphanum().length(24).required(),
});

module.exports = {idSchema};
