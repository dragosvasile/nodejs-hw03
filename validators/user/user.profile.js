const Joi = require('joi');

const changePassSchema = Joi.object({
  oldPassword: Joi.string().required(),
  newPassword: Joi.string().required(),
});

module.exports = {changePassSchema};
