const Joi = require('joi');

const postUserSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
  role: Joi.string().valid('SHIPPER', 'DRIVER').insensitive().required(),
});

const loginUserSchema = Joi.object({
  email: Joi.string().required(),
  password: Joi.string().required(),
});

const forgotPassSchema = Joi.object({
  email: Joi.string().required(),
});

module.exports = {
  postUserSchema,
  loginUserSchema,
  forgotPassSchema,
};
