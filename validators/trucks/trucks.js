const Joi = require('joi');

const truckTypeSchema = Joi.object({
  type: Joi.string().required(),
});

module.exports = {
  truckTypeSchema,
};
