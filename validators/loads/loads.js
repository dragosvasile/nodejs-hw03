const Joi = require('joi');

const Load = require('../../models/load');
const states = Load.schema.path('status').enumValues;

const getLoadsQuerySchema = Joi.object({
  status: Joi.string().valid(...states),
  limit: Joi.number().max(50).default(10),
  offset: Joi.number(),
});

const addLoadSchema = Joi.object({
  name: Joi.string().required(),
  payload: Joi.number().required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: {
    width: Joi.number().required(),
    length: Joi.number().required(),
    height: Joi.number().required(),
  },
});

module.exports = {
  getLoadsQuerySchema,
  addLoadSchema,
};
