const mongoose = require('mongoose');
const {Schema} = mongoose;

const truckSchema = new Schema({
  created_by: String,
  assigned_to: {type: String, default: ''},
  type: {type: String, enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT']},
  status: {type: String, enum: ['OL', 'IS'], default: 'OL'},
  created_date: {type: Date, default: Date.now()},
}, {versionKey: false});

const Truck = mongoose.model('Truck', truckSchema);

module.exports = Truck;
