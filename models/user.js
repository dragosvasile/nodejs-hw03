const mongoose = require('mongoose');
const {Schema} = mongoose;

const userSchema = new Schema({
  email: String,
  password: String,
  role: {type: String, enum: ['SHIPPER', 'DRIVER']},
  created_date: {type: Date, default: Date.now()},
}, {versionKey: false});

const User = mongoose.model('User', userSchema);

module.exports = User;
