const mongoose = require('mongoose');
const {Schema} = mongoose;

const loadSchema = new Schema({
  created_by: String,
  assigned_to: {type: String, default: ''},
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW'},
  state: {
    type: String,
    enum: [
      'En route to Pick Up',
      'Arrived to Pick Up',
      'En route to delivery',
      'Arrived to delivery'],
    default: 'En route to Pick Up'},
  name: String,
  payload: Number,
  pickup_address: String,
  delivery_address: String,
  dimensions: {
    width: {type: Number},
    length: {type: Number},
    height: {type: Number},
  },
  created_date: {type: Date, default: Date.now()},
  logs: [
    {
      message: {type: String},
      time: {type: Date, default: Date.now()},
    },
  ],
}, {versionKey: false});

const Load = mongoose.model('Load', loadSchema);

module.exports = Load;
