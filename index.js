require('dotenv').config();
require('./config/database').connect();
const express = require('express');
const app = express();
const {PORT} = process.env;
const morgan = require('morgan');

const errorHandler = require('./middlewares/errorHandler');
const auth = require('./middlewares/auth');
const fs = require('fs');

const authRouter = require('./routes/authRouter');
const userRouter = require('./routes/userRouter');
const truckRouter = require('./routes/truckRouter');
const loadRouter = require('./routes/loadRouter');
const imagesRouter = require('./routes/imagesRouter');
const reportsRouter = require('./routes/reportsRouter');
const weatherRouter = require('./routes/weatherRouter');

app.use(morgan('tiny', {
  stream: fs.createWriteStream('./log.txt', {flags: 'a'}),
}));

app.use(express.json());

app.use('/api/auth', authRouter);

app.use('/api/users', auth, userRouter);

app.use('/api/trucks', auth, truckRouter);

app.use('/api/loads', auth, loadRouter);

app.use('/api/images', auth, imagesRouter);

app.use('/api/reports', auth, reportsRouter);

app.use('/api/weather', auth, weatherRouter);

app.use(errorHandler);

app.listen(PORT, (req, res) => {
  console.log(`Server started on port ${PORT}`);
});

