const jwt = require('jsonwebtoken');
const {TOKEN_KEY} = process.env;
const CustomErr = require('../utils/customErr');


const verifyToken = (req, res, next) => {
  const token = req.headers['jwt_token'];
  if (!token) {
    return next(new CustomErr('A token is required for authentication', 400));
  }
  try {
    const decode = jwt.verify(token, TOKEN_KEY);
    req.user = decode;
  } catch (error) {
    return next(new CustomErr('Invalid token', 400));
  }
  return next();
};

module.exports = verifyToken;
