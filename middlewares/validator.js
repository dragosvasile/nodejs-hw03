module.exports = (Validators) => async (req, res, next) => {
  try {
    if ('body' in Validators) {
      const validated = await Validators['body'].validateAsync(req.body);
      req.body = validated;
    }
    if ('params' in Validators) {
      const validated = await Validators['params'].validateAsync(req.params);
      req.params = validated;
    }
    if ('query' in Validators) {
      const validated = await Validators['query'].validateAsync(req.query);
      req.query = validated;
    }
    return next();
  } catch (error) {
    return next(error);
  };
};
