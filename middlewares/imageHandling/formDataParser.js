const multer = require('multer');

const CustomError = require('../../utils/customErr');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './uploads/');
  },
  filename: (req, file, cb) => {
    cb(null, req.user.userId +
      file.originalname.slice(file.originalname.lastIndexOf('.')));
  },
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(new CustomError('File is not of accepted type', 400), false);
  }
};

// store all files in uploads folder
const upload = multer({
  storage: storage,
  limits: {
  // max size 5MB
    fileSize: 1024 * 1024 * 5,
  },
  fileFilter: fileFilter,
});

module.exports = {upload};
