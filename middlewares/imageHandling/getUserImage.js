const Image = require('../../models/image');

const getUserImage = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const existingImage = await Image.findOne({userId});
    if (!existingImage) {
      return next(new CustomErr(`User has no images uploaded`, 400));
    }
    existingImage.imageURL = existingImage.imageURL
        .slice(existingImage.imageURL.indexOf('\\'))
        .replace('\\', '/');
    req.url += existingImage.imageURL;
    return next();
  } catch (error) {
    return next(error);
  };
};

module.exports = {getUserImage};
