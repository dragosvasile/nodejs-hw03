const CustomErr = require('../utils/customErr');

const grantAccess = (userRole) => (req, res, next) => {
  try {
    const {role} = req.user;
    if (role === userRole) {
      return next();
    }
    return next(new CustomErr('You do not have permission', 400));
  } catch (error) {
    return next(error);
  }
};

module.exports = grantAccess;
