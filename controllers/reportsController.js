const CustomError = require('../utils/customErr');

const Load = require('../models/load');
const {generateReport} = require('../scripts/generateReport');


const getReport = async (req, res, next) => {
  try {
    const shippedLoads = await Load.find({status: 'SHIPPED'});
    if (!shippedLoads) {
      return next(new CustomError('There are no shipped loads', 400));
    };
    const stream = await generateReport(shippedLoads);
    res.attachment(`${Date.now()}-shipped-loads.xlsx`);
    return stream.pipe(res);
  } catch (error) {
    return next(error);
  }
};

module.exports = {getReport};
