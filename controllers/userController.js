const bcrypt = require('bcryptjs');
const CustomError = require('../utils/customErr');

const User = require('../models/user');

const getUser = async (req, res, next) => {
  try {
    const {userId} = req.user;

    const foundUser = await User.findOne({_id: userId});

    if (!foundUser) {
      return next(new CustomError('User not found', 400));
    };
    return res.status(200).json({
      user: {
        _id: foundUser._id,
        role: foundUser.role,
        email: foundUser.email,
        created_date: foundUser.created_date,
      },
    });
  } catch (error) {
    return next(error);
  }
};

const deleteUser = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const foundUser = await User.findOneAndDelete({_id: userId});

    if (!foundUser) {
      return next(new CustomError('This profile does not exist.', 400));
    };
    // add something here to prevent further accessing the token if deleted
    req.user = null;
    return res.status(200).json({message: 'Profile deleted successfully'});
  } catch (error) {
    return next(error);
  }
};

const changePass = async (req, res, next) => {
  try {
    const {userId} = req.user;

    const {oldPassword, newPassword} = req.body;
    const oldUser = await User.findOne({_id: userId});

    if (oldUser && (await bcrypt.compare(oldPassword, oldUser.password))) {
      const saltRounds = await bcrypt.genSalt(10);
      const encNewPass = await bcrypt.hash(newPassword, saltRounds);
      await User.updateOne(
          {_id: userId},
          {password: encNewPass});
      return res.status(200).json({message: 'Password changed successfully'});
    } else {
      return next(new CustomError('Old password does not match', 400));
    };
  } catch (error) {
    return next(error);
  }
};

module.exports ={
  getUser,
  deleteUser,
  changePass,
};
