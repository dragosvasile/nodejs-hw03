/* eslint-disable camelcase */
const CustomError = require('../utils/customErr');

const Load = require('../models/load');
const User = require('../models/user');
const Truck = require('../models/truck');

const getLoads = async (req, res, next) => {
  try {
    const {limit=10, offset=0, status={$exists: true}} = req.query;
    const {userId, role} = req.user;
    let loads = '';
    if (role === 'DRIVER') {
      loads = await Load
          .find({assigned_to: userId});
    }
    if (role === 'SHIPPER') {
      loads = await Load
          .find({created_by: userId});
    }

    loads = await Load.aggregate([
      {$match: {status}},
      {$skip: Number(offset)},
      {$limit: Number(limit)},
    ]);
    return res.status(200).json({loads});
  } catch (error) {
    return next(error);
  };
};

const addLoad = async (req, res, next) => {
  try {
    const message = 'Load created succesfully.';
    const {userId} = req.user;
    const {
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions} = req.body;
    await Load.create({
      created_by: userId,
      assigned_to: '',
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
      $push: {logs: {message}},
    });
    console.log(message);
    return res.status(200).json({message});
  } catch (error) {
    return next(error);
  }
};

const getActiveLoad = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const activeLoad = await Load.find({
      assigned_to: userId,
      status: 'ASSIGNED'});
    if (!activeLoad) {
      return next(new CustomError('There are no active loads for this user',
          400));
    }
    return res.status(200).json({load: activeLoad});
  } catch (error) {
    return next(error);
  }
};

const nextLoadState = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const load = await Load.findOne({assigned_to: userId});
    const states = Load.schema.path('state').enumValues;
    const nextState = states[states.indexOf(load.state) + 1];
    if (!nextState) {
      return next(new CustomError('State does not exist', 400));
    }
    const message = `Load changed state to ${nextState}`;
    if (nextState === 'Arrived to delivery') {
      await load.updateOne(
          {status: 'SHIPPED',
            state: nextState,
            $push: {logs: {message}},
          });
    } else {
      await load.updateOne(
          {state: nextState,
            $push: {logs: {message}},
          });
    }
    console.log(message);
    return res.status(200)
        .json({message});
  } catch (error) {
    return next(error);
  }
};

const getUserLoadsById = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const load = await Load.findOne({created_by: userId});
    if (!load) {
      return next(
          new CustomError('There are no loads created by this user', 400));
    }
    return res.status(200).json(load);
  } catch (error) {
    return next(error);
  }
};

const updateUserLoad = async (req, res, next) => {
  try {
    const message = 'Load details changed succesfully.';
    const {id} = req.params;
    const {
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions} = req.body;

    const updatedLoad = await Load.findOneAndUpdate(
        {_id: id,
          status: 'NEW'},
        {name,
          payload,
          pickup_address,
          delivery_address,
          dimensions,
          $push: {logs: {message}},
        });
    if (!updatedLoad) {
      return next(new CustomError(`There is no load with id ${id}`, 400));
    }
    console.log(message);
    return res.status(200).json({message});
  } catch (error) {
    return next(error);
  }
};

const deleteLoad = async (req, res, next) => {
  try {
    const {id} = req.params;
    const deletedLoad = Load.findOneAndDelete({_id: id, status: 'NEW'});
    if (!deletedLoad) {
      return next(new CustomError(`There is no load with id ${id}`, 400));
    }
    return res.status(200).json({message: 'Load deleted succesfully'});
  } catch (error) {
    return next(error);
  }
};

const postUserLoad = async (req, res, next) => {
  // driver can have only one load ASSIGNED
  // driver needs to have a truck
  try {
    const {id} = req.params;

    // check if load is already assigned to a driver
    const load = await Load.findOne({_id: id,
      $expr: {$gt: [{$strLenCP: '$assigned_to'}, 0]},
    });
    if (load) {
      return next(new CustomError(`Load with id ${
        id} is already assigned to a driver`, 400));
    }

    const allDriversIds = await User.find({role: 'DRIVER'}, '_id');
    const assignedLoads = await Load.find({status: 'ASSIGNED'});
    // getting trucks that have assigned users
    const assignedTrucks = await Truck.find(
        {$expr: {$gt: [{$strLenCP: '$assigned_to'}, 0]}},
        'assigned_to');
    // get arrays of only ids
    const driversWithAssignedTrucks = assignedTrucks
        .map((item) => item.assigned_to);
    const driversWithAssignedLoads = assignedLoads
        .map((item) => item.assigned_to);
    // filter for drivers with no load assigned && have a truck
    const availableDrivers = allDriversIds.filter((driver) =>
      !driversWithAssignedLoads.includes(driver._id.toString()) &&
      driversWithAssignedTrucks.includes(driver._id.toString()));

    if (availableDrivers.length < 1) {
      const postedLoad = await Load.findOneAndUpdate(
          {_id: id},
          {status: 'POSTED',
            $push: {logs: {message: `Load changed state to POSTED`}},
          });
      if (!postedLoad) {
        return next(new CustomError(`There is no load with id ${id}`, 400));
      }
      console.log(`Load changed state to POSTED`);
      return res.status(400).json({
        message: 'Load not assigned',
        driver_found: false,
      });
    }
    // if there is an available driver execute below
    const randomDriver = availableDrivers[
        Math.floor(Math.random() * availableDrivers.length) || 0];
    await Load.findOneAndUpdate(
        {_id: id},
        {assigned_to: randomDriver._id,
          status: 'ASSIGNED',
          $push: {logs: {message:
             `Load assigned to driver with id ${randomDriver._id}`}},
        });
    console.log(`Load assigned to driver with id ${randomDriver._id}`);
    return res.status(200).json({
      message: 'Load posted succesfully',
      driver_found: true,
    });
  } catch (error) {
    return next(error);
  }
};

const getLoadShippingInfo = async (req, res, next) => {
  try {
    const {id} = req.params;
    const load = await Load.findOne({_id: id});

    if (!load) {
      return next(new CustomError(`There is no load with id ${id}`, 400));
    }
    return res.status(200).json({load});
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  getLoads,
  addLoad,
  getActiveLoad,
  nextLoadState,
  getUserLoadsById,
  updateUserLoad,
  deleteLoad,
  postUserLoad,
  getLoadShippingInfo,
};
