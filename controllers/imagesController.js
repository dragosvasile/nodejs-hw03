const Image = require('../models/image');
const CustomErr = require('../utils/customErr');

const addFile = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const existingImage = await Image.findOne({userId});
    if (existingImage) {
      return next(new CustomErr(`User has already uploaded an image`, 400));
    }
    await Image.create({
      imageURL: req.file.path,
      userId,
    });
    return res.status(200).send({message: 'Image uploaded succesfully'});
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  addFile,
};
