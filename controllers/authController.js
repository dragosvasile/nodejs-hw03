const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {TOKEN_KEY} = process.env;

const CustomError = require('../utils/customErr');

const User = require('../models/user');

const registerUser = async (req, res, next) => {
  try {
    const {email, password, role} = req.body;

    const saltRounds = await bcrypt.genSalt(10);
    const encPass = await bcrypt.hash(password, saltRounds);

    if (await User.findOne({email})) {
      return next(new CustomError('This email has already been used', 400));
    }
    await User.create({
      email: email,
      password: encPass,
      // toUpperCase not really necessary beacuse the model handles this
      role: role.toUpperCase(),
    });

    return res.status(200).json({message: 'Profile created successfully'});
  } catch (error) {
    return next(new CustomError(error));
  }
};

const loginUser = async (req, res, next) => {
  try {
    const {email, password} = req.body;

    const foundUser = await User.findOne({email});

    if (foundUser && (await bcrypt.compare(password, foundUser.password))) {
      const token = jwt.sign({
        userId: foundUser._id,
        email: foundUser.email,
        role: foundUser.role},
      TOKEN_KEY, {
        expiresIn: '2h',
      });
      foundUser.token = token;

      return res.status(200).json({jwt_token: token});
    } else {
      return next(new CustomError('Username and/or password is invalid.', 400));
    };
  } catch (error) {
    return next(error);
  }
};

const forgotPass = async (req, res, next) => {
  try {
    const {email} = req.body;
    if (await User.findOne({email})) {
      return res.status(200).json({
        message: 'New password sent to your email address',
      });
    } else {
      return next(new CustomError(`No user with email ${email} found.`, 400));
    };
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  registerUser,
  loginUser,
  forgotPass,
};
