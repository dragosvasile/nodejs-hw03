const fetch = require('node-fetch');
const {WEATHER_KEY} = process.env;

const getWeather = async (req, res, next) => {
  try {
    const {city} = req.params;
    // getting location coordinates
    const locationData = await fetch(`http://api.openweathermap.org/geo/1.0/direct?q=${city}&limit=${1}&appid=${WEATHER_KEY}`);
    const location = await locationData.json();
    // getting weather information
    const weatherData = await fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${location[0]['lat']}&lon=${location[0]['lon']}&units=metric&appid=${WEATHER_KEY}`);
    const weather = await weatherData.json();

    const currentWeather = {
      location: city,
      weather,
    };
    res.status(200).json({weather: currentWeather});
  } catch (error) {
    return next(error);
  }
};


module.exports = {getWeather};
