const CustomError = require('../utils/customErr');

const Truck = require('../models/truck');

const getTrucks = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const userTrucks = await Truck.find({created_by: userId});
    return res.status(200).json({trucks: userTrucks});
  } catch (error) {
    return next(error);
  }
};

const addTruck = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const {type} = req.body;
    await Truck.create({
      created_by: userId,
      type,
    });

    return res.status(200).json({message: 'Truck created succesfully'});
  } catch (error) {
    return next(error);
  }
};

const getTruckById = async (req, res, next) => {
  try {
    const {id} = req.params;
    const truck = await Truck.findOne({_id: id});
    if (!truck) {
      return next(new CustomError('Truck does not exist', 400));
    }
    return res.status(200).json({truck});
  } catch (error) {
    return next(error);
  }
};

const updateTruck = async (req, res, next) => {
  try {
    const {id} = req.params;
    const {type} = req.body;
    const updatedTruck = await Truck.findOneAndUpdate({_id: id}, {type});
    if (!updatedTruck) {
      return next(new CustomError(`Truck with id ${id} not found`, 400));
    }
    return res
        .status(200)
        .json({message: 'Truck details changed successfully'});
  } catch (error) {
    return next(error);
  }
};

const deleteTruck = async (req, res, next) => {
  try {
    const {id} = req.params;
    const deletedTruck = await Truck.findOneAndDelete({_id: id});
    if (!deletedTruck) {
      return next(new CustomError(`Truck with id ${id} not found`, 400));
    }
    return res
        .status(200)
        .json({message: 'Truck deleted successfully'});
  } catch (error) {
    return next(error);
  }
};

const assignTruck = async (req, res, next) => {
  // what if truck is already assigned to a user?
  try {
    const {id} = req.params;
    const {userId} = req.user;
    const updatedTruck = await Truck.findOneAndUpdate({_id: id},
        {assigned_to: userId});
    if (!updatedTruck) {
      return next(new CustomError(`Truck with id ${id} not found`, 400));
    }
    return res
        .status(200)
        .json({message: 'Truck assigned successfully'});
  } catch (error) {
    return next(error);
  }
};


module.exports = {getTrucks,
  addTruck,
  getTruckById,
  updateTruck,
  deleteTruck,
  assignTruck,
};
