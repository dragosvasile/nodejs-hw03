const express = require('express');
// eslint-disable-next-line new-cap
const authRouter = express.Router();
const {registerUser,
  loginUser,
  forgotPass} = require('../controllers/authController');
const validator = require('../middlewares/validator');
const {
  postUserSchema,
  loginUserSchema,
  forgotPassSchema,
} = require('../validators/user/auth.user');


authRouter.route('/register')
    .post(
        validator({body: postUserSchema}),
        registerUser);
authRouter.route('/login')
    .post(
        validator({body: loginUserSchema}),
        loginUser);
authRouter.route('/forgot_password')
    .post(
        validator({body: forgotPassSchema}),
        forgotPass);

module.exports = authRouter;

