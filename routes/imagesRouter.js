const express = require('express');
// eslint-disable-next-line new-cap
const imagesRouter = express.Router();

const {
  addFile,
} = require('../controllers/imagesController');

const auth = require('../middlewares/auth');
const {upload} = require('../middlewares/imageHandling/formDataParser');
const {getUserImage} = require('../middlewares/imageHandling/getUserImage');


imagesRouter.route('/').post(upload.single('file'), addFile);

// profile photos are available from uploads directory
imagesRouter.use('/uploads', express.static('uploads'));


// logged in user can request his profile picture
imagesRouter.use(
    '/',
    auth,
    getUserImage,
    express.static('uploads'),
);


module.exports = imagesRouter;
