const express = require('express');
// eslint-disable-next-line new-cap
const truckRouter = express.Router();
const {getTrucks,
  addTruck,
  getTruckById,
  updateTruck,
  deleteTruck,
  assignTruck} = require('../controllers/truckController');

const grantAccess = require('../middlewares/grantAccess');
const validator = require('../middlewares/validator');
const {truckTypeSchema} = require('../validators/trucks/trucks');
const {idSchema} = require('../validators/database.id');

truckRouter.use(grantAccess('DRIVER'));

truckRouter.route('/')
    .get(getTrucks)
    .post(
        validator({body: truckTypeSchema}),
        addTruck);
truckRouter.route('/:id')
    .get(
        validator({params: idSchema}),
        getTruckById)
    .put(
        validator({params: idSchema, body: truckTypeSchema}),
        updateTruck)
    .delete(validator({params: idSchema}),
        deleteTruck);
truckRouter.route('/:id/assign')
    .post(
        validator({params: idSchema}),
        assignTruck);

module.exports = truckRouter;
