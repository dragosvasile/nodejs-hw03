const express = require('express');
// eslint-disable-next-line new-cap
const reportsRouter = express.Router();

const {
  getReport,
} = require('../controllers/reportsController');

reportsRouter.route('/').get(getReport);

module.exports = reportsRouter;
