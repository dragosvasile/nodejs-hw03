const express = require('express');
// eslint-disable-next-line new-cap
const loadRouter = express.Router();
const {
  getLoads,
  addLoad,
  getActiveLoad,
  nextLoadState,
  getUserLoadsById,
  updateUserLoad,
  deleteLoad,
  postUserLoad,
  getLoadShippingInfo,
} = require('../controllers/loadController');

const grantAccess = require('../middlewares/grantAccess');
const validator = require('../middlewares/validator');
const {idSchema} = require('../validators/database.id');
const {
  getLoadsQuerySchema,
  addLoadSchema,
} = require('../validators/loads/loads');

loadRouter.route('/')
    .get(
        validator({query: getLoadsQuerySchema}),
        getLoads)
    .post(
        grantAccess('SHIPPER'),
        validator({body: addLoadSchema}),
        addLoad);

loadRouter.route('/active').get(grantAccess('DRIVER'), getActiveLoad);

loadRouter.route('/active/state').patch(grantAccess('DRIVER'), nextLoadState);

loadRouter.use(grantAccess('SHIPPER'))
    .route('/:id')
    .get(
        validator({params: idSchema}),
        getUserLoadsById)
    .put(
        validator({
          body: addLoadSchema,
          params: idSchema}),
        updateUserLoad)
    .delete(
        validator({params: idSchema}),
        deleteLoad);

loadRouter.route('/:id/post').post(validator({params: idSchema}), postUserLoad);

loadRouter.route('/:id/shipping_info')
    .get(validator({params: idSchema}), getLoadShippingInfo);

module.exports = loadRouter;
