const express = require('express');
// eslint-disable-next-line new-cap
const userRouter = express.Router();
const {
  getUser,
  deleteUser,
  changePass,
} = require('../controllers/userController');

const validator = require('../middlewares/validator');
const {changePassSchema} = require('../validators/user/user.profile');

userRouter.route('/me').get(getUser).delete(deleteUser);

userRouter.route('/me/password')
    .patch(
        validator({body: changePassSchema}),
        changePass);

module.exports = userRouter;
