const express = require('express');
// eslint-disable-next-line new-cap
const weatherRouter = express.Router();
const {getWeather} = require('../controllers/weatherController');


weatherRouter.route('/:city').get(getWeather);

module.exports = weatherRouter;
